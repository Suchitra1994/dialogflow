def get_context(queryResult, name, session):
    context = None
    if (
        queryResult
        and queryResult["outputContexts"]
        and len(queryResult["outputContexts"]) > 0
    ):
        for i_context in queryResult["outputContexts"]:
            if i_context["name"] == "{session}/contexts/{name}".format(
                session=session, name=name
            ):
                context = i_context
    return context