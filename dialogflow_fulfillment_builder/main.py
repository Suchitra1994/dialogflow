import dialogflow_fulfillment_builder.helpers.context as context_helper


class Main:
    """
    * The object of this DialogflowFulfillment class can be used to call all the functions of this class
    * @param {Object} request Dialogflow Fulfillment request object
    * @example
    * {
    *      "responseId": "{RESPONSE_ID}",
    *      "queryResult": {
    *          "queryText": "Hello",
    *          "action": "input.welcome",
    *          "parameters": {},
    *          "allRequiredParamsPresent": true,
    *          "fulfillmentText": "Welcome",
    *          "fulfillmentMessages": [{ "text": { "text": [ "Welcome" ] } }],
    *          "outputContexts": [],
    *          "intent": {
    *              "name": "projects/{PROJECT_ID}/agent/intents/{INTENT_ID}",
    *              "displayName": "Default Welcome Intent"
    *          },
    *          "intentDetectionConfidence": 1,
    *          "languageCode": "en",
    *          "sentimentAnalysisResult": {
    *              "queryTextSentiment": {
    *                  "score": 0.1,
    *                  "magnitude": 0.1
    *              },
    *          },
    *      },
    *      "originalDetectIntentRequest": {
    *          "payload": {}
    *      },
    *      "session": "projects/{PROJECT_ID}/agent/sessions/{SESSION_ID}"
    * }
    """

    def __init__(self, request):
        self._request = request
        self._response = {
            "fulfillmentText": "",
            "fulfillmentMessages": [],
            "outputContexts": [],
        }

    """
     * set_response_text sets text response
     * @param {String} responseText the text to be set in the response (required)
     * @example
     * "Hello, welcome."
    """

    def set_response_text(self, response_text):
        self._response["fulfillmentMessages"].append(
            {"text": {"text": [response_text]}}
        )
        self._response["fulfillmentText"] = response_text

    """
     * get_context returns the outputContext object of the context name given in the input
     * @param {String} name Name of the context (required)
     * @example "context-name"
     * @returns {Object} Output Context Object
     * @example 
     *  {
     *      "name": "context-name",
     *      "lifespanCount": 2,
     *      "parameters": {
     *          "username": "abc"
     *          "username.original": "abc"
     *      }
     *  }
     """

    def get_context(self, name):
        if self._request and self._request["queryResult"]:
            return context_helper.get_context(
                self._request["queryResult"], name, self._request["session"]
            )
        return None

    """
     * set_output_context sets output contexts
     * @param {String} name Name of the context (required)
     * @example "Context Name"
     * @param {Number} lifespan Lifespan of the context (required)
     * @example 5
     * @param {Object} parameters Parameters to be associated with the context (optional)
     * @example 
     *  {
     *      "username": "abc"
     *  }
     """

    def set_output_context(self, name, lifespan, parameters={}):
        push_context_in_response = True
        context = context_helper.get_context(
            self._request["queryResult"], name, self._request["session"]
        )
        if context_helper.get_context(self._response, name, self._request["session"]):
            context = context_helper.get_context(
                self._response, name, self._request["session"]
            )
            push_context_in_response = False
        if context:
            context["parameters"] = (
                context["parameters"] if context["parameters"] else {}
            )
            for key in parameters:
                context["parameters"][key] = parameters[key]
            if push_context_in_response:
                self._response["outputContexts"].append(context)
        else:
            template = {
                "name": "{session}/contexts/{name}".format(
                    session=self._request["session"], name=name
                ),
                "lifespanCount": lifespan,
                "parameters": parameters,
            }
            self._response["outputContexts"].append(template)

    """
     * set_event sets the followupEventInput
     * @param {String} name Name of the Event (required)
     * @example "Followup Event"
     * @param {String} languageCode (optional)
     * @example "en-US"
     * @param {Object} parameters (optional)
     * @example
     *  {
     *      "username": "abc"
     *  }
    """

    def set_event(self, name, languageCode="en-US", parameters={}):
        self._response["followupEventInput"] = {
            "name": name,
            "languageCode": languageCode,
            "parameters": parameters,
        }

    def get_compiled_response(self):
        return self._response
