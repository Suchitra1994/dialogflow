from flask import Flask, request, make_response, jsonify
from dotenv import load_dotenv
from intent_library.webhook_controller import webhook_controller
from settings import env
from functools import wraps
from flask import Flask, render_template, request, make_response, Response
import os

app = Flask(__name__)




def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwards):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwards)
    return decorated

def check_auth(username, password):
    return username == os.getenv('secret-username') and password == os.getenv('secret-password')

def authenticate():
    return Response(
    'Invalid login.\n'
    'Invalid login.', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})



@app.route("/healthcheck", methods=["GET"])
def healthcheck():
    return make_response(jsonify({"message": "ok"}))


@app.route("/v2beta1/webhook", methods=["POST"])
@requires_auth
def webhook():
    request_data = request.json
    response = webhook_controller(request_data)
    return make_response(jsonify(response))


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
