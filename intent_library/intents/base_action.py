from abc import abstractmethod


class BaseAction:
    """
    * Base Class
    * All the intent actions should extend this Base actions class
    * All the Child class should write implemenation for abstract method @action to set response, set output contexts etc.
    """

    def __init__(self):
        super()

    @abstractmethod
    def action(self, df):
        pass

    def run(self, df):
        self.action(df)