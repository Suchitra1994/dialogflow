from .base_action import BaseAction
from data.responses import responses


class Inputdetails(BaseAction):
    """
    * Default Welcome Intent controller
    * @param {object} df webhook fulfillment object"""

    def action(self, df):
        intent = df._request["queryResult"]["intent"]["displayName"]
        response = responses[intent]
        df.set_response_text(response)
