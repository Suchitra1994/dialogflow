"""
 * Maps dialogflow intent with its controller
"""
intents = {
    "Default Welcome Intent": {
        "module": "intent_library.intents.default_welcome_intent",
        "class": "DefaultWelcomeIntent",
    },
    "Inputdetails": {
        "module": "intent_library.intents.Inputdetails",
        "class": "Inputdetails",
    }
}
