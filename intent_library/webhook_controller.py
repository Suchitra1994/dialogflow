import traceback
import importlib
from .intent_mapper import intents
from dialogflow_fulfillment_builder.main import Main as dialogflow_fulfillment


def webhook_controller(request_data):
    try:
        fulfillment = dialogflow_fulfillment(request_data)
        request_intent = request_data["queryResult"]["intent"]["displayName"]
        intent_action_class_instance = instantiate_intent_action_class(request_intent)
        intent_action_class_instance.run(fulfillment)
        response = fulfillment.get_compiled_response()
        return response
    except Exception as e:
        print(traceback.format_exc())


def instantiate_intent_action_class(request_intent):
    intent_action_module_path = intents[request_intent]["module"]
    intent_action_class_name = intents[request_intent]["class"]
    intent_action_module = importlib.import_module(intent_action_module_path)
    intent_action_class = getattr(intent_action_module, intent_action_class_name)
    intent_action_class_instance = intent_action_class()
    return intent_action_class_instance
